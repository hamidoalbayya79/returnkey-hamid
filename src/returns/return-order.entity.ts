import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Item, ReturnStatus } from './return-order.model';

@Entity()
export class ReturnOrder {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  orderId: string;

  @Column()
  email: string;

  @Column('json', { nullable: true })
  item: Item[];

  @Column({ type: 'real', default: 0 })
  estimatedAmount: string;

  @Column({ type: 'real', default: 0 })
  currentAmount: number;

  @Column()
  status: ReturnStatus;
}
