import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CreateReturnOrderDto } from './dto/create-return-order.dto';
import { GetTokenDto } from './dto/get-token.dto';
import { UpdateReturnQcDto } from './dto/update-return-qc.dto';
import { ReturnOrder } from './return-order.entity';
import { ReturnOrdersService } from './return-orders.service';

@Controller()
export class ReturnOrdersController {
  constructor(private returnOrdersService: ReturnOrdersService) {}

  @Get('/returns')
  getAllReturns(): Promise<ReturnOrder[]> {
    return this.returnOrdersService.getAllReturns();
  }

  @Get('/returns/:id')
  getReturnById(@Param('id') id: string) {
    return this.returnOrdersService.getReturnById(id);
  }

  @Post('/pending/returns')
  getToken(@Body() getTokenDto: GetTokenDto): Promise<{ accessToken: string }> {
    return this.returnOrdersService.getToken(getTokenDto);
  }

  @Post('/returns')
  @UseGuards(AuthGuard())
  createReturn(
    @Body() createReturnOrdersDto: CreateReturnOrderDto,
  ): Promise<ReturnOrder> {
    return this.returnOrdersService.createReturn(createReturnOrdersDto);
  }

  @Patch('/returns/:id/items/:itemId/qc/status')
  updateReturnItemQC(
    @Param('id') id: string,
    @Param('itemId') itemId: string,
    @Body() updateReturnQcDto: UpdateReturnQcDto,
  ): Promise<ReturnOrder> {
    const { status } = updateReturnQcDto;
    return this.returnOrdersService.updateReturnItemQC(id, itemId, status);
  }
}
