export interface Item {
  itemId: string;
  quantity: number;
  price: number;
  qualityControl: QualityControl;
}
export enum ReturnStatus {
  AWAITING_APPROVAL = 'AWAITING_APPROVAL',
  COMPLETE = 'COMPLETE',
}

export enum QualityControl {
  PENDING = 'PENDING',
  ACCEPTED = 'ACCEPTED',
  REJECTED = 'REJECTED',
}
