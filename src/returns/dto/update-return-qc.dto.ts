import { IsEnum } from 'class-validator';
import { QualityControl } from '../return-order.model';

export class UpdateReturnQcDto {
  @IsEnum(QualityControl)
  status: QualityControl;
}
