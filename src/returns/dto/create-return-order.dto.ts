import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';

class Item {
  @IsNotEmpty()
  itemId: string;

  @IsNotEmpty()
  quantity: number;
}

export class CreateReturnOrderDto {
  @IsNotEmpty()
  orderId: string;

  @IsNotEmpty()
  email: string;

  @ValidateNested()
  @Type(() => Item)
  item: Item[];
}
