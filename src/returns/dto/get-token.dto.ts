import { IsNotEmpty, IsString } from 'class-validator';

export class GetTokenDto {
  @IsString()
  @IsNotEmpty()
  orderId: string;

  @IsString()
  @IsNotEmpty()
  email: string;
}
