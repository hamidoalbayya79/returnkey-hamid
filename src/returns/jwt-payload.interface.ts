export interface JwtPayload {
  orderId: string;
  email: string;
}
