import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { QualityControl, ReturnStatus } from './return-order.model';

import { CreateReturnOrderDto } from './dto/create-return-order.dto';
import * as fs from 'fs';
import { Repository } from 'typeorm';
import { ReturnOrder } from './return-order.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { GetTokenDto } from './dto/get-token.dto';

@Injectable()
export class ReturnOrdersService {
  constructor(
    @InjectRepository(ReturnOrder)
    private returnOrdersRepository: Repository<ReturnOrder>,
    private jwtService: JwtService,
  ) {}

  getOrders() {
    const results = [];
    let i = 0;
    const dataFile = fs
      .readFileSync(__dirname + '/../../orders.csv', 'utf8')
      .split('\r\r\n');
    for (const data of dataFile) {
      if (i === 0) {
        i++;
        continue;
      }
      const arr = data.split(',');
      const obj = { ...arr };
      results.push(obj);
      i++;
    }

    return results;
  }

  private async validateReturnOrder(req: CreateReturnOrderDto, items: any[]) {
    let amount = 0;
    const products = [];

    if (!req.item) {
      const checkReturn = await this.returnOrdersRepository.findOne({
        where: { orderId: req.orderId, email: req.email },
      });

      if (checkReturn) {
        throw new HttpException(
          `Return request for Order ID ${req.orderId} is ${checkReturn.status}`,
          HttpStatus.FORBIDDEN,
        );
      }
    } else {
      const getReturnOrder = await this.returnOrdersRepository.findOne({
        where: { orderId: req.orderId, email: req.email },
      });

      if (getReturnOrder) {
        const found = getReturnOrder.item.find((i) =>
          req.item.find((e) => e.itemId === i.itemId),
        );

        if (found) {
          throw new HttpException(
            `Return request for item ${found.itemId} is ${getReturnOrder.status}`,
            HttpStatus.FORBIDDEN,
          );
        }
      }
    }

    items.forEach((item) => {
      if (item[0] === req.orderId && item[1] === req.email) {
        if (!req.item) {
          amount = amount + item[3] * item[4];
          products.push({
            itemId: item[2],
            quantity: item[3],
            price: item[4],
            qualityControl: QualityControl.PENDING,
          });
        } else {
          req.item.forEach((i) => {
            if (i.itemId === item[2]) {
              if (i.quantity > item[3]) {
                throw new HttpException(
                  `The quantity to be returned for product ${i.itemId} is higher than the quantity purchased`,
                  HttpStatus.FORBIDDEN,
                );
              }
              amount = amount + i.quantity * item[4];
              products.push({
                itemId: i.itemId,
                quantity: i.quantity,
                price: item[4],
                qualityControl: QualityControl.PENDING,
              });
            }
          });
        }
      }
    });

    return { products, amount };
  }

  async getAllReturns(): Promise<ReturnOrder[]> {
    return await this.returnOrdersRepository.find();
  }

  async getReturnById(id: string) {
    const returnOrder = await this.returnOrdersRepository.findOne({
      select: ['status', 'item', 'currentAmount'],
      where: { id },
    });

    if (!returnOrder) {
      throw new NotFoundException(`Return with Order ID ${id} not found`);
    }

    if (returnOrder.status === ReturnStatus.COMPLETE) {
      const newItems = returnOrder.item.filter(
        (item) => item.qualityControl === QualityControl.ACCEPTED,
      );
      returnOrder.item = newItems;
    }

    return returnOrder;
  }

  async getToken(getTokenDto: GetTokenDto): Promise<{ accessToken: string }> {
    const payload = getTokenDto;
    const orders = this.getOrders();
    let match = 0;

    for (const order of orders) {
      if (getTokenDto.orderId === order[0] && getTokenDto.email === order[1]) {
        match += 1;
      }
    }

    if (match === 0) {
      throw new NotFoundException(
        `No order found with ID ${payload.orderId} and email ${payload.email}`,
      );
    }

    const accessToken: string = await this.jwtService.sign(payload);

    return { accessToken };
  }

  async createReturn(
    createReturnOrdersDto: CreateReturnOrderDto,
  ): Promise<ReturnOrder> {
    const { orderId, email } = createReturnOrdersDto;

    const items = this.getOrders();

    const order = await this.validateReturnOrder(createReturnOrdersDto, items);

    const returnOrder: ReturnOrder = this.returnOrdersRepository.create({
      orderId,
      email,
      item: order.products,
      estimatedAmount: order.amount.toString(),
      currentAmount: 0,
      status: ReturnStatus.AWAITING_APPROVAL,
    });

    await this.returnOrdersRepository.save(returnOrder);

    return returnOrder;
  }

  async updateReturnItemQC(
    id: string,
    itemId: string,
    status: QualityControl,
  ): Promise<ReturnOrder> {
    if (status === QualityControl.PENDING) {
      throw new HttpException(
        `Status ${QualityControl.PENDING} is unacceptable`,
        HttpStatus.FORBIDDEN,
      );
    }

    const returnOrder = await this.returnOrdersRepository.findOne({
      where: { id },
    });

    if (!returnOrder) {
      throw new NotFoundException(`Return with Order ID ${id} not found`);
    }

    const foundItem = returnOrder.item.find((i) => i.itemId === itemId);

    if (!foundItem) {
      throw new NotFoundException(
        `Item with Item ID ${itemId} in Order ${id} not found`,
      );
    }

    returnOrder.item.forEach((i) => {
      if (i.itemId === itemId) {
        i.qualityControl = status;
        if (status === QualityControl.ACCEPTED) {
          returnOrder.currentAmount =
            returnOrder.currentAmount + i.quantity * i.price;
        }
      }
    });

    const checkStatus = returnOrder.item.find(
      (i) => i.qualityControl === QualityControl.PENDING,
    );

    if (!checkStatus) {
      returnOrder.status = ReturnStatus.COMPLETE;
    }

    await this.returnOrdersRepository.save(returnOrder);
    return returnOrder;
  }
}
