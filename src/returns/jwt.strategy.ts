import { Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { JwtPayload } from './jwt-payload.interface';
import { ReturnOrdersService } from './return-orders.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private returnOrdersService: ReturnOrdersService,
    private configService: ConfigService,
  ) {
    super({
      secretOrKey: configService.get('JWT_SECRET'),
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    });
  }

  async validate(payload: JwtPayload) {
    const { orderId, email } = payload;

    const orders = await this.returnOrdersService.getOrders();

    let order = {};

    for (const orderObj of orders) {
      if (orderId === orderObj[0] && email === orderObj[1]) {
        order = orderObj;
      }
    }

    if (!order) {
      throw new NotFoundException(
        `No order found with ID ${payload.orderId} and email ${payload.email}`,
      );
    }

    return order;
  }
}
