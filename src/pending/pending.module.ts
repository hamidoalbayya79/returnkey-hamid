import { Module } from '@nestjs/common';
import { PendingService } from './pending.service';
import { PendingController } from './pending.controller';

@Module({
  providers: [PendingService],
  controllers: [PendingController],
})
export class PendingModule {}
