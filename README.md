# Test Abdul Hamid for Full Stack Developer in ReturnKey

## Endpoints

- getToken (Get Token) - http://localhost:3000/pending/returns (HTTP Post) to get the token for creating new return request (will return JWT and it will expire in 1 hour)

```sh
## Get Token Example

## Endpoint (HTTP Post)
http://localhost:3000/pending/returns

## Request Body
{
    "orderId": "RK-478",
    "email": "john@example.com"
}


## Response Body
{
    "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcmRlcklkIjoiUkstNDc4IiwiZW1haWwiOiJqb2huQGV4YW1wbGUuY29tIiwiaWF0IjoxNjYwOTg4MzEzLCJleHAiOjE2NjA5OTE5MTN9.iZ9HX5UTo-05RILpqJlFmUgBb1lVj3dAN1hA_hrFEqQ"
}
```

- createReturn (Create Return) - http://localhost:3000/returns (HTTP Post) to create new return (require a JWT in the Auth Header with Bearer that you can get by executing getToken Endpoint)

```sh
## Create Return Example

## Endpoint (HTTP Post)
http://localhost:3000/returns

## Request Body
{
    "orderId": "RK-478",
    "email": "john@example.com",
    "item": [
        {
            "itemId": "NIKE-7",
            "quantity": "1"
        }
    ]
}

Note that there\'s an item property with the item detail to return specific item or item with specific quantity.
remove the item property if you want to return all the products instead.


## Response Body
{
    "orderId": "RK-478",
    "email": "john@example.com",
    "item": [
        {
            "itemId": "NIKE-7",
            "quantity": "1",
            "price": "110.75",
            "qualityControl": "PENDING"
        }
    ],
    "estimatedAmount": 110.75,
    "currentAmount": 0,
    "status": "AWAITING_APPROVAL",
    "id": "c55fd2ec-b00f-4af0-afb1-93a97a6ff62e"
}
```

- getAllReturns (Get All Returns) - http://localhost:3000/returns (HTTP Get) to get all returns from database

```sh
## Get All Returns Example

## Endpoint (HTTP Get)
http://localhost:3000/returns

## Response Body
[
    {
        "id": "4800ba12-f8e9-47ca-9af9-815451508903",
        "orderId": "RK-478",
        "email": "john@example.com",
        "item": [
            {
                "itemId": "MENS-156",
                "quantity": "1",
                "price": "50",
                "qualityControl": "ACCEPTED"
            }
        ],
        "estimatedAmount": 50,
        "currentAmount": 50,
        "status": "COMPLETE"
    },
    {
        "id": "df14f8bc-7567-42f6-bf9c-2db8f66717dc",
        "orderId": "RK-478",
        "email": "john@example.com",
        "item": [
            {
                "itemId": "NIKE-7",
                "quantity": "1",
                "price": "110.75",
                "qualityControl": "PENDING"
            }
        ],
        "estimatedAmount": 110.75,
        "currentAmount": 110.75,
        "status": "COMPLETE"
    },
]
```

- getReturnById (Get Return By ID) http://localhost:3000/returns/:returnId (HTTP Get) to get a return from database with a specific id

```sh
## Get Return By ID Example

## Endpoint (HTTP Get)
http://localhost:3000/returns/df14f8bc-7567-42f6-bf9c-2db8f66717dc

## Response Body
{
    "item": [
        {
            "itemId": "NIKE-7",
            "quantity": "1",
            "price": "110.75",
            "qualityControl": "ACCEPTED"
        }
    ],
    "currentAmount": 221.5,
    "status": "COMPLETE"
}
```

- updateReturnItemQc (Update Return Item QC) - http://localhost:3000/returns/:returnId/items/:itemId/qc/status (HTTP Patch) to update the Quality Control of the returned item (I'm using Patch HTTP method to make it clearer that this endpoint only update partial data instead of all the data)

```sh
## Update Return Item Quality Control Example

## Endpoint (HTTP Patch)
http://localhost:3000/returns/df14f8bc-7567-42f6-bf9c-2db8f66717dc/items/NIKE-7/qc/status

## Request Body
{
    "status": "ACCEPTED"
}


## Response Body
{
    "id": "df14f8bc-7567-42f6-bf9c-2db8f66717dc",
    "orderId": "RK-478",
    "email": "john@example.com",
    "item": [
        {
            "itemId": "NIKE-7",
            "quantity": "1",
            "price": "110.75",
            "qualityControl": "ACCEPTED"
        }
    ],
    "estimatedAmount": 110.75,
    "currentAmount": 332.25,
    "status": "COMPLETE"
}
```

I also attached a postman collection in the root folder of this project to make the test much easier and faster, also note that for the token im using Authentication Header with Bearer.

## Tech

This app is using techs below :

- Javascript - The programming language
- NodeJs - The runtime
- NestJs - The framework
- PostgreSQL - The database (You can use docker to run the PostgreSQL instance to make it easier)
- TypeORM - The ORM for interaction between the Backend and the Database
- jsonwebtoken - To do authorization check, whether a user has a valid token or not to create a new return
- PassportJs - To make the jwt validation easier and shorter
- Joi - To validate the app's configuration schema

## Installation

Make sure to install all the dependencies first by running below command from the command line in the root folder of this app

```sh
yarn install
```

Also you need to create a postgres database with name "return-order" without quotes and leave it blank, since all the tables and relations will be automatically created by the app when it starts for the first time. If you have Docker you can run an instance of PostgreSQL by using Docker and create the database by using PgAdmin, you can simply run the command below to create the PostgreSQL instance.

```sh
docker run --name return-order -p 5432:5432 -e POSTGRES_PASSWORD=postgres -d postgres
```

## Running The App

Once everything is done you can start the node server simply by running this command :

```sh
yarn start:dev
```

## Note

This is just a simple application to do CRUD operations for returning order item and checking the return status. You need to execute the "getToken" endpoint to get a JWT that will be required when you want to create a new return.
